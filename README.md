datalife_sale_kit_shipment_move_price
=====================================

The sale_kit_shipment_move_price module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-sale_kit_shipment_move_price/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-sale_kit_shipment_move_price)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
