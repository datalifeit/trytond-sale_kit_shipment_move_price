# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    @classmethod
    def update_unit_price(cls, moves):
        sale2kit_products = {}
        moves_ = moves.copy()
        for move in moves_:
            origin = move.origin
            if (origin and origin.__name__ == 'sale.line'
                    and origin.unit_price == 0):
                sale = origin.sale
                # Get all kit line products from all the sale kits.
                if sale not in sale2kit_products:
                    products = set()
                    for line in sale.lines:
                        if not (
                                line.product
                                and line.product.kit
                                and line.product.explode_kit_in_sales
                                and line.product.type == 'service'):
                            continue
                        products |= set(kl.product
                            for kl in line.product.kit_lines)
                    sale2kit_products[sale] = products
                if move.product in sale2kit_products[sale]:
                    moves_.remove(move)
        super().update_unit_price(moves_)
