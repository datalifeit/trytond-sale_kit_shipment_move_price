# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import sale
from . import move


def register():
    Pool.register(
        sale.Sale,
        move.Move,
        module='sale_kit_shipment_move_price', type_='model')
    Pool.register(
        sale.SaleValued,
        module='sale_kit_shipment_move_price', type_='model',
        depends=['stock_valued'])
