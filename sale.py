# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool, PoolMeta
from decimal import Decimal
from itertools import groupby

ZERO = Decimal(0)

class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    def create_shipment(self, shipment_type):
        pool = Pool()
        Move = pool.get('stock.move')

        shipments = super().create_shipment(shipment_type)
        if shipment_type != 'out':
            return shipments
        if not shipments:
            return

        kits = dict()
        kit_lines = set([l for l in self.lines if l.product
            and l.product.kit
            and l.product.explode_kit_in_sales
            and l.product.type == 'service'
        ])
        kit_lines = {sale_line: set([
            (l.product, l.quantity)
            for l in sale_line.product.kit_lines])
            for sale_line in kit_lines
        }

        for shipment in shipments:
            remaining_moves = set(shipment.outgoing_moves)
            for kit_sale_line, kit_products in kit_lines.items():
                kit_products = [kp[0] for kp in kit_products]
                moves = [m for m in remaining_moves
                    if m.product in kit_products
                    and m.origin
                    and m.origin.__name__ == 'sale.line'
                    and not m.unit_price
                ]
                if not moves:
                    continue
                kits.setdefault(kit_sale_line, set()).update(moves)
                remaining_moves -= set(moves)

        for kit_sale_line, moves in kits.items():
            moves = sorted(list(moves), key=lambda m: m.product)
            total_amount = sum(product.list_price * Decimal(str(qty))
                for product, qty in kit_lines[kit_sale_line])
            for product, grouped_moves in groupby(moves,
                    key=lambda m: m.product):
                grouped_moves = list(grouped_moves)
                total_quantity = sum(m.quantity for m in grouped_moves)
                for move in grouped_moves:
                    pct = (move.quantity / total_quantity)
                    amount_pct = ((product.list_price
                        * Decimal(str(move.quantity)) / total_amount)
                        if total_amount else ZERO)

                    move.unit_price = (Decimal(str(pct))
                       * kit_sale_line.unit_price * amount_pct
                        / Decimal(str(move.quantity))).quantize(
                        Decimal(1) / 10 ** Move.unit_price.digits[1])
                    move.save()
        return shipments


class SaleValued(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    def create_shipment(self, shipment_type):
        pool = Pool()
        Move = pool.get('stock.move')

        shipments = super().create_shipment(shipment_type)
        if shipment_type != 'out':
            return shipments
        if not shipments:
            return
        to_save = []
        for shipment in shipments:
            for move in shipment.inventory_moves:
                if (move.origin.__name__ == 'stock.move'
                        and move.origin.origin.__name__ == 'sale.line'
                        and not move.unit_price
                        and move.origin.unit_price
                        and move.origin.origin.kit_depth):
                    move.unit_price = move.origin.unit_price
                    to_save.append(move)
        if to_save:
            Move.save(to_save)
